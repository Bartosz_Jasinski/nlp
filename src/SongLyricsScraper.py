import os
import csv
from bs4 import BeautifulSoup, SoupStrainer
from urllib.request import Request, urlopen


def getSongGenre():
    working_directory = "../data/billboard_ranking_with_song_genre"
    os.chdir(working_directory)
    # billboard_ranking_directory = "../billboard_ranking"
    # billboard_ranking_with_song_genre_directory = "../billboard_ranking_with_song_genre"

    first_rank_year = 1960
    # first_rank_year = 2000
    # last_rank_year = 2011
    last_rank_year = 1961
    for rank_year in range(first_rank_year, last_rank_year):
        printPrettyInfoAboutProcessedPage(rank_year)
        createRankingWithLyrics(rank_year)
        # song_title = artist_song_title_tuple[0]
        # artist = artist_song_title_tuple[1]
        # print('artist ', artist, '     song name ', song_title)
    # last_fm_url = genreSongLyricsUrl(artist, song_title)
    # last_fm_response = requests.post(last_fm_url)
    # d = json.loads(last_fm_response.text)
    # print(json.dumps(d, indent=4, sort_keys=True))


def printPrettyInfoAboutProcessedPage(rank_year):
    print('---------------------', rank_year, '---------------------', '\n')


def createRankingWithLyrics(rank_year):
    ranking_data_file_name = "BillboardRankingWithLyrics" + str(rank_year) + ".csv"
    songlyrics_url = genrateSongLyricsUrl(rank_year)
    scrapPage(songlyrics_url, ranking_data_file_name, rank_year)
    # ### DEBUG ###########
    # print(songlyrics_url, "\n")
    # ### DEBUG ###########


def scrapPage(songlyrics_url, ranking_data_file_name, rank_year):
    print(songlyrics_url)
    page = Request(songlyrics_url, headers={'User-Agent': 'Mozilla/5.0'})
    #only_tr_tags = SoupStrainer('tr')
    soup = BeautifulSoup(urlopen(page).read(), 'lxml')#, parse_only=only_tr_tags
    lyrics_html = soup.find("table", {"class": "tracklist"})
    # ############DEBUG
    # print(lyrics_html.get_text())
    # ############DEBUG
    with open(ranking_data_file_name, 'w', encoding='utf-8') as ranking_file:
        csv_ranking_file_writer = csv.writer(ranking_file, delimiter='~')
        csv_ranking_file_writer.writerow(["Rank", "Song Name", "Artist Name", "Genre", "Year", "Lyrics", "Source"])
        for row in lyrics_html.findAll("tr"):
            try:
                cells = row.findAll("td")
                rank_number = cells[0].get_text()
                artist_name = cells[1].find("a").get_text()
                song_name = cells[2].find("a").get_text()
                song_lyrics = "INVALID"
                genre = "INVALID"
                try:
                    song_url = cells[2].find('a').attrs['href']
                    artist_url = cells[1].find('a').attrs['href']
                    song_lyrics_genre_tuple = scrapLyricsAndGenre(song_url, artist_url)
                    song_lyrics = song_lyrics_genre_tuple[0]
                    genre = song_lyrics_genre_tuple[1]
                    csv_ranking_file_writer.writerow(
                        [rank_number, song_name, artist_name, genre, str(rank_year), song_lyrics,
                         genrateSongLyricsUrl(rank_year)])
                    # ############DEBUG
                    # print(cells, "\n")
                    print(rank_number, "\n")
                    print(song_name, "\n")
                    print(artist_name, "\n")
                    print(genre, "\n")
                    print(str(rank_year), "\n")
                    print(song_lyrics, '\n')
                    # print("SONG URL ", song_url, "\n")
                    # print("ARTIST URL ", artist_url, "\n")
                    # ############DEBUG
                except:
                    csv_ranking_file_writer.writerow(
                        [rank_number, song_name, artist_name, genre, str(rank_year), song_lyrics,
                         genrateSongLyricsUrl(rank_year)])
            except:
                pass


def scrapLyricsAndGenre(song_url, artist_url):
    page = Request(song_url, headers={'User-Agent': 'Mozilla/5.0'})
    soup = BeautifulSoup(urlopen(page).read(), 'lxml')#, parse_only=SoupStrainer['p', 'div']
    lyrics = scrapLyrics(soup)
    genre = scrapGenre(soup, artist_url)

    return lyrics, genre


def scrapLyrics(soup):
    lyrics_html = soup.find("p", {"class": "songLyricsV14"})
    return lyrics_html.get_text()


def scrapGenre(soup, artist_url):
    try:
        return scrapGenreFromLyricsPage(soup)
    except:
        # print(traceback.format_exc())
        return scrapGenreFromArtistPage(artist_url)


def scrapGenreFromLyricsPage(soup):
    genre_html = soup.find("div", {"class": "pagetitle"}).findAll("p")[2].find('a')
    return genre_html.get_text()


def scrapGenreFromArtistPage(artist_url):
    page = Request(artist_url, headers={'User-Agent': 'Mozilla/5.0'})
    soup = BeautifulSoup(urlopen(page).read(), 'lxml')#, parse_only=SoupStrainer('div')
    genre_html = soup.find("div", {"class": "pagetitle"}).find("a")
    return genre_html.get_text()


def genrateSongLyricsUrl(year):
    return 'https://www.songlyrics.com/news/top-songs/' + str(year) + '/'


getSongGenre()


# def getRidOfUnnecessaryCharacters(string):
#     intab = " .',()éÿ-"
#     outtab = "---      "
#     cleared_string = string.translate(str.maketrans(intab, outtab))
#     return cleared_string.replace(" ", "")


# def getArtistsAndSongsTitles(ranking_data_file_name):
#     with open(ranking_data_file_name) as billboard_ranking:
#         billboard_ranking_reader = csv.DictReader(billboard_ranking, delimiter=';')
#         artists_songs_titles_list = []
#         for row in billboard_ranking_reader:
#             artists_songs_titles_list.append((row['Song Name'], row['Artist Name']))
#         # ### DEBUG ###########
#         # print(artists_songs_titles_list)
#         # ### DEBUG ###########
#     return artists_songs_titles_list












# import wikipedia
# import os
# from bs4 import BeautifulSoup
#
#
# def iterateThroughAllBillboardRankings():
#     working_directory = "../data/billboard_ranking"
#     os.chdir(working_directory)
#
#     first_rank_year = 1960
#     # first_rank_year = 1980
#     last_rank_year = 2018
#     for rank_year in range(first_rank_year, last_rank_year):
#         printPrettyInfoAboutProcessedPage(rank_year)
#         ranking_table_html = getWikipediaBillboardRankForGivenYear(rank_year)
#         ranking_data_file_name = "BillboardRanking" + str(rank_year) + ".csv"
#         ranking_data_file = open(ranking_data_file_name, 'w', encoding='utf-8')
#         data_scheme = "Billboard Ranking Place;Song Name;Artist Name\n"
#         ranking_data_file.write(data_scheme)
#         createRankFile(ranking_data_file, ranking_table_html)
#
#
# def printPrettyInfoAboutProcessedPage(rank_year):
#     print('---------------------', rank_year, '---------------------', '\n')
#
#
# def getWikipediaBillboardRankForGivenYear(rank_year):
#     wikipedia_page_title = 'Billboard Year-End Hot 100 singles of ' + str(rank_year)
#     billboard_ranking = wikipedia.page(wikipedia_page_title)
#     page_text = billboard_ranking.html()
#     soup = BeautifulSoup(page_text, "html.parser")
#     ranking_table_html = soup.find("table", {"class": "wikitable sortable"})
#     return ranking_table_html
#
#
# def createRankFile(ranking_data_file, ranking_table_html):
#     for row in ranking_table_html.findAll("tr"):
#         cells = row.findAll("td")
#         after_1981_cells = row.findAll("th")  # It is necessary shit
#         # For each "tr", assign each "td" to a variable.
#         rank = ""
#         title = ""
#         artist = ""
#         if len(cells) == 3:
#             rank = cells[0].find(text=True)
#             title = cells[1].findAll(text=True)
#             title = title[int((len(title) - 1) / 2)]
#             artist = cells[2].find(text=True)
#             if title.startswith('"') and title.endswith('"'):
#                 title = title[1:-1]
#
#         elif len(cells) == 2:  # Tables are written inconsistent compare
#             # https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1981 and
#             # https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1982
#             rank = after_1981_cells[0].find(text=True)
#             rank = rank.strip()
#             title = cells[0].findAll(text=True)
#             title = title[int((len(title) - 1) / 2)]
#             artist = cells[1].find(text=True)
#             artist = artist.strip()
#             if title.startswith('"') and title.endswith('"'):
#                 title = title[1:-1]
#
#         record = rank + ';' + title + ';' + artist + '\n'
#         # record = record.decode('utf-8', 'ignore').encode("utf-8")
#         if len(rank) != 0 or len(title) != 0 or len(artist) != 0:
#             ranking_data_file.write(record)
#
#     ranking_data_file.close()
# 4
# # !!!!!!!!!!!!!!!!!!DEBUG!!!!!!!!!!!!!!!!!!
# #    print("\n \n \n \n \n \n \n \n \n \n \n \n")
# #    print(ranking_table_html)
# # !!!!!!!!!!!!!!!!!!DEBUG!!!!!!!!!!!!!!!!!!
#
#
# # def clearScrapedStrings(string):
#
#
# iterateThroughAllBillboardRankings()















































# import requests
# import json
# import os
# import csv
# from bs4 import BeautifulSoup
# # import urllib.request
# from urllib.request import Request, urlopen
#
# def getSongGenre():
#     working_directory = "../data/billboard_ranking"
#     os.chdir(working_directory)
#     billboard_ranking_directory = "../billboard_ranking"
#     billboard_ranking_with_song_genre_directory = "../billboard_ranking_with_song_genre"
#
#     first_rank_year = 1960
#     last_rank_year = 2018
#     for rank_year in range(first_rank_year, last_rank_year):
#         printPrettyInfoAboutProcessedPage(rank_year)
#         ranking_data_file_name = "BillboardRanking" + str(rank_year) + ".csv"
#         artist_song_title_list = getArtistsAndSongsTitles(ranking_data_file_name)
#         createRankingWithLyrics(artist_song_title_list)
#         # song_title = artist_song_title_tuple[0]
#         # artist = artist_song_title_tuple[1]
#         # print('artist ', artist, '     song name ', song_title)
#     # last_fm_url = genreSongLyricsUrl(artist, song_title)
#     # last_fm_response = requests.post(last_fm_url)
#     # d = json.loads(last_fm_response.text)
#     # print(json.dumps(d, indent=4, sort_keys=True))
#
#
# def printPrettyInfoAboutProcessedPage(rank_year):
#     print('---------------------', rank_year, '---------------------', '\n')
#
#
# def getArtistsAndSongsTitles(ranking_data_file_name):
#     with open(ranking_data_file_name) as billboard_ranking:
#         billboard_ranking_reader = csv.DictReader(billboard_ranking, delimiter=';')
#         artists_songs_titles_list = []
#         for row in billboard_ranking_reader:
#             artists_songs_titles_list.append((row['Song Name'], row['Artist Name']))
#         # ### DEBUG ###########
#         # print(artists_songs_titles_list)
#         # ### DEBUG ###########
#     return artists_songs_titles_list
#
#
# def createRankingWithLyrics(artist_song_title_list):
#     for artist_song_record in artist_song_title_list:
#         songlyrics_url = genreSongLyricsUrl(artist_song_record[0], artist_song_record[1])
#         # ### DEBUG ###########
#         # print(songlyrics_url, "\n")
#         # ### DEBUG ###########
#         lyrics = scrapLyrics(songlyrics_url)
#
#
# def scrapLyrics(songlyrics_url):
#     print(songlyrics_url)
#     page = Request(songlyrics_url, headers={'User-Agent': 'Mozilla/5.0'})
#     soup = BeautifulSoup(urlopen(page).read(), "html.parser")
#     lyrics_html = soup.find("p", {"class": "songLyricsV14"})
#     print(lyrics_html.get_text())
#     # for lyrics_html in soup.find("p", href=True):
#     #     print
#     #     anchor['href']
#
#
# def genreSongLyricsUrl(song_title, artist):
#     return 'https://www.songlyrics.com/' + getRidOfUnnecessaryCharacters(artist).lower() + '/' \
#            + getRidOfUnnecessaryCharacters(song_title).lower() + '-lyrics/'
#
#
# def getRidOfUnnecessaryCharacters(string):
#     intab = " .',()éÿ-"
#     outtab = "---      "
#     cleared_string = string.translate(str.maketrans(intab, outtab))
#     return cleared_string.replace(" ", "")
#
#
# getSongGenre()






















#
#
#
#
# import wikipedia
# import os
# from bs4 import BeautifulSoup
#
#
# def iterateThroughAllBillboardRankings():
#     working_directory = "../data/billboard_ranking"
#     os.chdir(working_directory)
#
#     first_rank_year = 1960
#     # first_rank_year = 1980
#     last_rank_year = 2018
#     for rank_year in range(first_rank_year, last_rank_year):
#         printPrettyInfoAboutProcessedPage(rank_year)
#         ranking_table_html = getWikipediaBillboardRankForGivenYear(rank_year)
#         ranking_data_file_name = "BillboardRanking" + str(rank_year) + ".csv"
#         ranking_data_file = open(ranking_data_file_name, 'w', encoding='utf-8')
#         createRankFile(ranking_data_file, ranking_table_html)
#
#
# def printPrettyInfoAboutProcessedPage(rank_year):
#     print('---------------------', rank_year, '---------------------', '\n')
#
#
# def getWikipediaBillboardRankForGivenYear(rank_year):
#     wikipedia_page_title = 'Billboard Year-End Hot 100 singles of ' + str(rank_year)
#     billboard_ranking = wikipedia.page(wikipedia_page_title)
#     page_text = billboard_ranking.html()
#     soup = BeautifulSoup(page_text, "html.parser")
#     ranking_table_html = soup.find("table", {"class": "wikitable sortable"})
#     return ranking_table_html
#
#
# def createRankFile(ranking_data_file, ranking_table_html):
#     for row in ranking_table_html.findAll("tr"):
#         cells = row.findAll("td")
#         after_1981_cells = row.findAll("th")  # It is necessary shit
#         # For each "tr", assign each "td" to a variable.
#         rank = ""
#         title = ""
#         artist = ""
#         if len(cells) == 3:
#             rank = cells[0].find(text=True)
#             title = cells[1].findAll(text=True)
#             title = title[int((len(title) - 1) / 2)]
#             artist = cells[2].find(text=True)
#             if title.startswith('"') and title.endswith('"'):
#                 title = title[1:-1]
#
#         elif len(cells) == 2:  # Tables are written inconsistent compare
#             # https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1981 and
#             # https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1982
#             rank = after_1981_cells[0].find(text=True)
#             rank = rank.strip()
#             title = cells[0].findAll(text=True)
#             title = title[int((len(title) - 1) / 2)]
#             artist = cells[1].find(text=True)
#             artist = artist.strip()
#             if title.startswith('"') and title.endswith('"'):
#                 title = title[1:-1]
#
#         record = rank + ',' + title + ',' + artist + '\n'
#         # record = record.decode('utf-8', 'ignore').encode("utf-8")
#         if len(rank) != 0 or len(title) != 0 or len(artist) != 0:
#             ranking_data_file.write(record)
#
#     ranking_data_file.close()
# 4
# # !!!!!!!!!!!!!!!!!!DEBUG!!!!!!!!!!!!!!!!!!
# #    print("\n \n \n \n \n \n \n \n \n \n \n \n")
# #    print(ranking_table_html)
# # !!!!!!!!!!!!!!!!!!DEBUG!!!!!!!!!!!!!!!!!!
#
#
# # def clearScrapedStrings(string):
#
#
# iterateThroughAllBillboardRankings()
