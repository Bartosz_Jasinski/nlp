import os
from wordcloud import WordCloud
import matplotlib.pyplot as plt


# Read the whole text.
billboard_ranking_songs_directory = "../../data/billboard_ranking_statistics"
os.chdir(billboard_ranking_songs_directory)
text = open('constitution.txt').read()

# Generate a word cloud image
wordcloud = WordCloud().generate(text)

# Display the generated image:
# the matplotlib way:
plt.imshow(wordcloud, interpolation='bilinear')
plt.axis("off")

# lower max_font_size
wordcloud = WordCloud(max_font_size=40).generate(text)
plt.figure()
plt.imshow(wordcloud, interpolation="bilinear")
plt.axis("off")
plt.show()

# The pil way (if you don't have matplotlib)
# image = wordcloud.to_image()
# image.show()