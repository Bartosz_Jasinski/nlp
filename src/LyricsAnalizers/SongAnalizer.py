from textstat.textstat import textstat
import os
import csv


def countSyllables():
    billboard_ranking_songs_directory = "../../data/own_songs"
    os.chdir(billboard_ranking_songs_directory)
    countSyllablesInYearRanking("Song1")


def countSyllablesInYearRanking(name):
    song_file = str(name) + ".csv"
    with open(song_file, 'r', encoding='utf-8') as song:
        csv_song_file_reader = csv.DictReader(song, delimiter='~')

        ranking_statistics_file_name = str(name) + "Statistics" + ".csv"
        with open(ranking_statistics_file_name, 'w', encoding='utf-8') as ranking_statistics_file:
            csv_ranking_file_writer = csv.writer(ranking_statistics_file, delimiter='~')
            csv_ranking_file_writer.writerow(["Genre", "Year", "Word Number", "Syllable Number",
                                              "Average Syllables Per Word", "Average Word Length", "Flesch Reading Ease",
                                              "Flesch-Kincaid Grade Level",  "Fog Scale", "Text Standard"])
            all_lyrics_for_genre = {}
            for row in csv_song_file_reader:
                if row["Genre"] in all_lyrics_for_genre.keys():
                    all_lyrics_for_genre[row["Genre"]] = all_lyrics_for_genre[row["Genre"]] + " " + row["Lyrics"]
                else:
                    all_lyrics_for_genre[row["Genre"]] = row["Lyrics"]

            for key in all_lyrics_for_genre:
                # print(textstat.lexicon_count(row))
                # print(textstat.syllable_count(row["Lyrics"]))
                # print(textstat.avg_syllables_per_word(row["Lyrics"]))
                # print(textstat.avg_letter_per_word(row["Lyrics"]))
                stats = [str(key), str(name),
                         str(textstat.lexicon_count(all_lyrics_for_genre[key])),
                         str(textstat.syllable_count(all_lyrics_for_genre[key])),
                         str(textstat.avg_syllables_per_word(all_lyrics_for_genre[key])),
                         str(textstat.avg_letter_per_word(all_lyrics_for_genre[key])),
                         str(textstat.flesch_reading_ease(all_lyrics_for_genre[key])),
                         str(textstat.flesch_kincaid_grade(all_lyrics_for_genre[key])),
                         str(textstat.gunning_fog(all_lyrics_for_genre[key])),
                         str(textstat.text_standard(all_lyrics_for_genre[key]))]
                csv_ranking_file_writer.writerow(stats)



countSyllables()
