# import matplotlib.pyplot as plt
# import numpy as np
# import os
#
# import matplotlib.cbook as cbook
#
# billboard_ranking_statistics_directory = "../../data/billboard_ranking_statistics"
# os.chdir(billboard_ranking_statistics_directory)
# print(os.getcwd())
# fname = cbook.get_sample_data('BillboardRankingStatisticsGenre2009.csv', asfileobj=False)
# # fname2 = cbook.get_sample_data('data_x_x2_x3.csv', asfileobj=False)
#
# # test 1; use ints
# plt.plotfile(fname)
#















# import glob
# import matplotlib.pyplot as plt
# import pandas as pd
#
# files = glob.glob('BillboardRankingStatisticsGenre1961.csv')
#
# for file in files:
#     df1=pd.read_csv(file,header=1,sep=',')
#     fig = plt.figure()
#     plt.subplot(2, 1, 1)
#     plt.plot(df1.iloc[:,[1]],df1.iloc[:,[2]])
#
#     plt.subplot(2, 1, 2)
#     plt.plot(df1.iloc[:,[3]],df1.iloc[:,[4]])
#     plt.show()

import os
import csv


def aggregateAverageWordLengthForYear(year):
    ranking_data_file_name = "BillboardRankingStatisticsGenre" + str(year) + ".csv"
    with open(ranking_data_file_name, 'r', encoding='utf-8') as ranking_file:
        csv_ranking_file_reader = csv.DictReader(ranking_file, delimiter='~')

        ranking_statistics_file_name = "BillboardRankingStatisticsAggregatedAverageWordLength.csv"
        with open(ranking_statistics_file_name, 'a', encoding='utf-8') as ranking_statistics_file:
            csv_ranking_file_writer = csv.writer(ranking_statistics_file, delimiter='~')
            for row in csv_ranking_file_reader:
                if row["Genre"] == "INVALID":
                    pass
                else:
                    csv_ranking_file_writer.writerow([str(year), row["Genre"], row["Average Word Length"]])


def aggregateAverageWordLength():
    ranking_statistics_file_name = "BillboardRankingStatisticsAggregatedAverageWordLength.csv"
    with open(ranking_statistics_file_name, 'a', encoding='utf-8') as ranking_statistics_file:
        csv_ranking_file_writer = csv.writer(ranking_statistics_file, delimiter='~')
        csv_ranking_file_writer.writerow(["Year", "Genre", "Average Word Length"])

    first_rank_year = 1960
    last_rank_year = 2011
    for year in range(first_rank_year, last_rank_year):
        aggregateAverageWordLengthForYear(year)

def aggregateFleschReadingEaseForYear(year):
    ranking_data_file_name = "BillboardRankingStatisticsGenre" + str(year) + ".csv"
    with open(ranking_data_file_name, 'r', encoding='utf-8') as ranking_file:
        csv_ranking_file_reader = csv.DictReader(ranking_file, delimiter='~')

        ranking_statistics_file_name = "BillboardRankingStatisticsAggregatedFleschReadingEase.csv"
        with open(ranking_statistics_file_name, 'a', encoding='utf-8') as ranking_statistics_file:
            csv_ranking_file_writer = csv.writer(ranking_statistics_file, delimiter='~')
            for row in csv_ranking_file_reader:
                if row["Genre"] == "INVALID":
                    pass
                else:
                    csv_ranking_file_writer.writerow([str(year), row["Genre"], row["Flesch Reading Ease"]])


def aggregateFleschReadingEase():
    ranking_statistics_file_name = "BillboardRankingStatisticsAggregatedFleschReadingEase.csv"
    with open(ranking_statistics_file_name, 'a', encoding='utf-8') as ranking_statistics_file:
        csv_ranking_file_writer = csv.writer(ranking_statistics_file, delimiter='~')
        csv_ranking_file_writer.writerow(["Year", "Genre", "Flesch Reading Ease"])

    first_rank_year = 1960
    last_rank_year = 2011
    for year in range(first_rank_year, last_rank_year):
        aggregateFleschReadingEaseForYear(year)


def aggregateStatistics():
    billboard_ranking_songs_directory = "../../data/billboard_ranking_statistics"
    os.chdir(billboard_ranking_songs_directory)

    aggregateWordsNumber()
    aggregateSyllableNumber()
    aggregateAverageSyllablesPerWord()
    aggregateAverageWordLength()
    aggregateFleschReadingEase()
    # aggregateFleschKincaidGradeLevel()
    # aggregateFogScale()
    # aggregateTextStandard()


def aggregateWordsNumber():
    ranking_statistics_file_name = "BillboardRankingStatisticsAggregatedWordsNumber.csv"
    with open(ranking_statistics_file_name, 'a', encoding='utf-8') as ranking_statistics_file:
        csv_ranking_file_writer = csv.writer(ranking_statistics_file, delimiter='~')
        csv_ranking_file_writer.writerow(["Year", "Genre", "Word Number"])

    first_rank_year = 1960
    last_rank_year = 2011
    for year in range(first_rank_year, last_rank_year):
        aggregateWordsNumberForYear(year)


def aggregateWordsNumberForYear(year):
    ranking_data_file_name = "BillboardRankingStatisticsGenre" + str(year) + ".csv"
    with open(ranking_data_file_name, 'r', encoding='utf-8') as ranking_file:
        csv_ranking_file_reader = csv.DictReader(ranking_file, delimiter='~')

        ranking_statistics_file_name = "BillboardRankingStatisticsAggregatedWordsNumber.csv"
        with open(ranking_statistics_file_name, 'a', encoding='utf-8') as ranking_statistics_file:
            csv_ranking_file_writer = csv.writer(ranking_statistics_file, delimiter='~')
            # "Syllable Number",
            # "Average Syllables Per Word", "Average Word Length"
            all_statistics_for_genre = {}
            for row in csv_ranking_file_reader:
                if row["Genre"] == "INVALID":
                    pass
                else:
                    csv_ranking_file_writer.writerow([str(year), row["Genre"], row["Word Number"]])

            # for key in all_statistics_for_genre:
            #     # print(textstat.lexicon_count(row))
            #     # print(textstat.syllable_count(row["Lyrics"]))
            #     # print(textstat.avg_syllables_per_word(row["Lyrics"]))
            #     # print(textstat.avg_letter_per_word(row["Lyrics"]))
            #     stats = [str(key), str(year),
            #              str(textstat.lexicon_count(all_statistics_for_genre[key])),
            #              str(textstat.syllable_count(all_statistics_for_genre[key])),
            #              str(textstat.avg_syllables_per_word(all_statistics_for_genre[key])),
            #              str(textstat.avg_letter_per_word(all_statistics_for_genre[key]))]
            #     csv_ranking_file_writer.writerow(stats)
            #
            # billboard_ranking_songs_directory = "../billboard_ranking_with_song_genre"
            # os.chdir(billboard_ranking_songs_directory)


def aggregateAverageSyllablesPerWordForYear(year):
    ranking_data_file_name = "BillboardRankingStatisticsGenre" + str(year) + ".csv"
    with open(ranking_data_file_name, 'r', encoding='utf-8') as ranking_file:
        csv_ranking_file_reader = csv.DictReader(ranking_file, delimiter='~')

        ranking_statistics_file_name = "BillboardRankingStatisticsAggregatedAverageSyllablesPerWord.csv"
        with open(ranking_statistics_file_name, 'a', encoding='utf-8') as ranking_statistics_file:
            csv_ranking_file_writer = csv.writer(ranking_statistics_file, delimiter='~')
            for row in csv_ranking_file_reader:
                if row["Genre"] == "INVALID":
                    pass
                else:
                    csv_ranking_file_writer.writerow([str(year), row["Genre"], row["Average Syllables Per Word"]])


def aggregateAverageSyllablesPerWord():
    ranking_statistics_file_name = "BillboardRankingStatisticsAggregatedAverageSyllablesPerWord.csv"
    with open(ranking_statistics_file_name, 'a', encoding='utf-8') as ranking_statistics_file:
        csv_ranking_file_writer = csv.writer(ranking_statistics_file, delimiter='~')
        csv_ranking_file_writer.writerow(["Year", "Genre", "Syllable Number"])

    first_rank_year = 1960
    last_rank_year = 2011
    for year in range(first_rank_year, last_rank_year):
        aggregateAverageSyllablesPerWordForYear(year)


def aggregateSyllableNumberForYear(year):
    ranking_data_file_name = "BillboardRankingStatisticsGenre" + str(year) + ".csv"
    with open(ranking_data_file_name, 'r', encoding='utf-8') as ranking_file:
        csv_ranking_file_reader = csv.DictReader(ranking_file, delimiter='~')

        ranking_statistics_file_name = "BillboardRankingStatisticsAggregatedSyllableNumber.csv"
        with open(ranking_statistics_file_name, 'a', encoding='utf-8') as ranking_statistics_file:
            csv_ranking_file_writer = csv.writer(ranking_statistics_file, delimiter='~')
            for row in csv_ranking_file_reader:
                if row["Genre"] == "INVALID":
                    pass
                else:
                    csv_ranking_file_writer.writerow([str(year), row["Genre"], row["Syllable Number"]])


def aggregateSyllableNumber():
    ranking_statistics_file_name = "BillboardRankingStatisticsAggregatedSyllableNumber.csv"
    with open(ranking_statistics_file_name, 'a', encoding='utf-8') as ranking_statistics_file:
        csv_ranking_file_writer = csv.writer(ranking_statistics_file, delimiter='~')
        csv_ranking_file_writer.writerow(["Year", "Genre", "Syllable Number"])

    first_rank_year = 1960
    last_rank_year = 2011
    for year in range(first_rank_year, last_rank_year):
        aggregateSyllableNumberForYear(year)


aggregateStatistics()
