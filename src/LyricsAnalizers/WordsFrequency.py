import os
import csv


def countWords():
    billboard_ranking_songs_directory = "../../data/billboard_ranking_with_song_genre"
    os.chdir(billboard_ranking_songs_directory)

    first_rank_year = 1960
    last_rank_year = 2011
    for year in range(first_rank_year, last_rank_year):
        countWordsInYearRanking(year)


def countWordsInYearRanking(year):
    ranking_data_file_name = "BillboardRankingWithLyrics" + str(year) + ".csv"
    with open(ranking_data_file_name, 'r', encoding='utf-8') as ranking_file:
        csv_ranking_file_reader = csv.DictReader(ranking_file, delimiter='~')
        # print(ranking_file.read())
        uniques = []
        all_texts = ''
        for row in csv_ranking_file_reader:
            all_texts += row["Lyrics"].lower() + "\n"
            # print(row["Lyrics"].lower().split(), "\n")
            for word in row["Lyrics"].lower().split():
                if word not in uniques:
                    uniques.append(word)

        counts = []
        for unique in uniques:
            count = 0              # Initialize the count to zero.
            for word in all_texts.split():     # Iterate over the words.
                if word == unique:   # Is this word equal to the current unique?
                    count += 1         # If so, increment the count
            counts.append((count, unique))

        counts.sort()            # Sorting the list puts the lowest counts first.
        counts.reverse()         # Reverse it, putting the highest counts first.

        billboard_ranking_statistics_directory = "../billboard_ranking_statistics"
        os.chdir(billboard_ranking_statistics_directory)
        ranking_statistics_file_name = "BillboardRankingStatistics" + str(year) + ".csv"
        ranking_statistics_file = open(ranking_statistics_file_name , 'w', encoding='utf-8')

        ranking_statistics_file.write("rank words frequency\n")

        for i in range(min(50, len(counts))):
            count, word = counts[i]
            # print('%s %d' % (word, count))
            ranking_statistics_file.write(str(i + 1) + " " + word + " " + str(count) + "\n")

        billboard_ranking_songs_directory = "../billboard_ranking_with_song_genre"
        os.chdir(billboard_ranking_songs_directory)


countWords()





# import os
# import csv
#
#
# def countWords():
#     working_directory = "../../data/billboard_ranking_with_song_genre"
#     os.chdir(working_directory)
#     ranking_data_file_name = "BillboardRankingWithLyrics" + str(1963) + ".csv"
#     with open(ranking_data_file_name, 'r', encoding='utf-8') as ranking_file:
#         csv_ranking_file_reader = csv.DictReader(ranking_file, delimiter='~')
#         # print(ranking_file.read())
#         uniques = []
#         for row in csv_ranking_file_reader:
#             # print(row["Lyrics"].lower().split(), "\n")
#             for word in row["Lyrics"].lower().split():
#                 if word not in uniques:
#                     uniques.append(word)
#
#         counts = []
#         for unique in uniques:
#             count = 0              # Initialize the count to zero.
#             for row in csv_ranking_file_reader:
#                 for word in row["Lyrics"].lower().split():     # Iterate over the words.
#                     if word == unique:   # Is this word equal to the current unique?
#                         count += 1         # If so, increment the count
#                 counts.append((count, unique))
#
#         counts.sort()            # Sorting the list puts the lowest counts first.
#         counts.reverse()         # Reverse it, putting the highest counts first.
#
#         for i in range(min(10, len(counts))):
#             count, word = counts[i]
#             print('%s %d' % (word, count))
#
#
# countWords()






# words = open('alice30.txt').read().lower().split()
#
#
# uniques = []
# for word in words:
#   if word not in uniques:
#     uniques.append(word)
#
# counts = []
# for unique in uniques:
#   count = 0              # Initialize the count to zero.
#   for word in words:     # Iterate over the words.
#     if word == unique:   # Is this word equal to the current unique?
#       count += 1         # If so, increment the count
#   counts.append((count, unique))
#
# counts.sort()            # Sorting the list puts the lowest counts first.
# counts.reverse()         # Reverse it, putting the highest counts first.
#
# for i in range(min(10, len(counts))):
#   count, word = counts[i]
#   print('%s %d' % (word, count))


