# from textstat.textstat import textstat
# import os
# import csv
# from rhyme_detect import scheme
# import re
#
#
# # def countSyllables():
# #     working_directory = "../../data/billboard_ranking_with_song_genre"
# #     os.chdir(working_directory)
# #     ranking_data_file_name = "BillboardRankingWithLyrics" + str(1963) + ".csv"
# #     with open(ranking_data_file_name, 'r', encoding='utf-8') as ranking_file:
# #         csv_ranking_file_reader = csv.DictReader(ranking_file, delimiter='~')
# #         for row in csv_ranking_file_reader:
# #             print(textstat.lexicon_count(row["Lyrics"]))
# #             print(textstat.syllable_count(row["Lyrics"]))
# #             print(textstat.avg_syllables_per_word(row["Lyrics"]))
# #             print(textstat.avg_letter_per_word(row["Lyrics"]))
# #             # print(scheme.from_lines(row["Lyrics"]))
# #             # print(scheme.from_lines(
# #             #     'My mistress\' eyes are nothing like the sun \n Coral is far more red than her lips\' red'
# #             #     'If snow be white, why then her breasts are dun'
# #             #     'If hairs be wires, black wires grow on her head'
# #             #
# #             #     'I have seen roses damask\'d, red and white'
# #             #     'But no such roses see I in her cheeks'
# #             #     'And in some perfumes is there more delight'
# #             #     'Than in the breath that from my mistress reeks'))
# #     #         print(scheme.from_lines([
# #     #     "My mistress' eyes are nothing like the sun",
# #     #     "Coral is far more red than her lips' red",
# #     #     "If snow be white, why then her breasts are dun",
# #     #     "If hairs be wires, black wires grow on her head",
# #     #
# #     #     "I have seen roses damask'd, red and white",
# #     #     "But no such roses see I in her cheeks",
# #     #     "And in some perfumes is there more delight",
# #     #     "Than in the breath that from my mistress reeks",
# #     #
# #     #     "I love to hear her speak, yet well I know",
# #     #     "That music hath a far more pleasing sound",
# #     #     "I grant I never saw a goddess go",
# #     #     "My mistress, when she walks, treads on the ground",
# #     #
# #     #     "And yet, by heaven, I think my love as rare",
# #     #     "As any she belied with false compare",
# #     # ]))
# #             # print(scheme.from_lines(str(row["Lyrics"]).splitlines()))
# #             # lines = []
# #             # for line in str(row["Lyrics"]).split('\n'):
# #             #     if line.strip() != '':
# #             #         lines += line
# #             # print(lines)
# #
# #             # print(str(re.match(r'^\s*$', str(row["Lyrics"])).splitlines()))
# #             # print("THrere's nice place ")
# #
# #             print("\n")
# #
# #
# # countSyllables()
#
#
# def countSyllables():
#     billboard_ranking_songs_directory = "../../data/billboard_ranking_with_song_genre"
#     os.chdir(billboard_ranking_songs_directory)
#
#     first_rank_year = 1960
#     last_rank_year = 2011
#     for year in range(first_rank_year, last_rank_year):
#         countSyllablesInYearRanking(year)
#
#
# def countSyllablesInYearRanking(year):
#     ranking_data_file_name = "BillboardRankingWithLyrics" + str(year) + ".csv"
#     with open(ranking_data_file_name, 'r', encoding='utf-8') as ranking_file:
#         csv_ranking_file_reader = csv.DictReader(ranking_file, delimiter='~')
#
#         billboard_ranking_statistics_directory = "../billboard_ranking_statistics"
#         os.chdir(billboard_ranking_statistics_directory)
#         ranking_statistics_file_name = "BillboardRankingStatisticsOther" + str(year) + ".csv"
#         with open(ranking_statistics_file_name, 'w', encoding='utf-8') as ranking_statistics_file:
#             csv_ranking_file_writer = csv.writer(ranking_statistics_file, delimiter='~')
#             csv_ranking_file_writer.writerow(["Rank", "Song Name", "Artist Name", "Genre", "Year", "Word Number",
#                                              "Syllable Number", "Average Syllables Per Word", "Average Word Length"])
#             for row in csv_ranking_file_reader:
#                 print(textstat.lexicon_count(row["Lyrics"]))
#                 # print(textstat.syllable_count(row["Lyrics"]))
#                 # print(textstat.avg_syllables_per_word(row["Lyrics"]))
#                 # print(textstat.avg_letter_per_word(row["Lyrics"]))
#                 stats = [str(row["Rank"]), str(row["Song Name"]), str(row["Artist Name"]), str(row["Genre"]), str(row["Year"]),
#                          str(textstat.lexicon_count(row["Lyrics"])), str(textstat.syllable_count(row["Lyrics"])),
#                          str(textstat.avg_syllables_per_word(row["Lyrics"])), str(textstat.avg_letter_per_word(row["Lyrics"]))]
#                 csv_ranking_file_writer.writerow(stats)
#
#             billboard_ranking_songs_directory = "../billboard_ranking_with_song_genre"
#             os.chdir(billboard_ranking_songs_directory)
#
#
# countSyllables()







from textstat.textstat import textstat
import os
import csv
from rhyme_detect import scheme
import re


# def countSyllables():
#     working_directory = "../../data/billboard_ranking_with_song_genre"
#     os.chdir(working_directory)
#     ranking_data_file_name = "BillboardRankingWithLyrics" + str(1963) + ".csv"
#     with open(ranking_data_file_name, 'r', encoding='utf-8') as ranking_file:
#         csv_ranking_file_reader = csv.DictReader(ranking_file, delimiter='~')
#         for row in csv_ranking_file_reader:
#             print(textstat.lexicon_count(row["Lyrics"]))
#             print(textstat.syllable_count(row["Lyrics"]))
#             print(textstat.avg_syllables_per_word(row["Lyrics"]))
#             print(textstat.avg_letter_per_word(row["Lyrics"]))
#             # print(scheme.from_lines(row["Lyrics"]))
#             # print(scheme.from_lines(
#             #     'My mistress\' eyes are nothing like the sun \n Coral is far more red than her lips\' red'
#             #     'If snow be white, why then her breasts are dun'
#             #     'If hairs be wires, black wires grow on her head'
#             #
#             #     'I have seen roses damask\'d, red and white'
#             #     'But no such roses see I in her cheeks'
#             #     'And in some perfumes is there more delight'
#             #     'Than in the breath that from my mistress reeks'))
#     #         print(scheme.from_lines([
#     #     "My mistress' eyes are nothing like the sun",
#     #     "Coral is far more red than her lips' red",
#     #     "If snow be white, why then her breasts are dun",
#     #     "If hairs be wires, black wires grow on her head",
#     #
#     #     "I have seen roses damask'd, red and white",
#     #     "But no such roses see I in her cheeks",
#     #     "And in some perfumes is there more delight",
#     #     "Than in the breath that from my mistress reeks",
#     #
#     #     "I love to hear her speak, yet well I know",
#     #     "That music hath a far more pleasing sound",
#     #     "I grant I never saw a goddess go",
#     #     "My mistress, when she walks, treads on the ground",
#     #
#     #     "And yet, by heaven, I think my love as rare",
#     #     "As any she belied with false compare",
#     # ]))
#             # print(scheme.from_lines(str(row["Lyrics"]).splitlines()))
#             # lines = []
#             # for line in str(row["Lyrics"]).split('\n'):
#             #     if line.strip() != '':
#             #         lines += line
#             # print(lines)
#
#             # print(str(re.match(r'^\s*$', str(row["Lyrics"])).splitlines()))
#             # print("THrere's nice place ")
#
#             print("\n")
#
#
# countSyllables()


def countSyllables():
    billboard_ranking_songs_directory = "../../data/billboard_ranking_with_song_genre"
    os.chdir(billboard_ranking_songs_directory)

    first_rank_year = 1960
    last_rank_year = 2011
    for year in range(first_rank_year, last_rank_year):
        countSyllablesInYearRanking(year)


def countSyllablesInYearRanking(year):
    ranking_data_file_name = "BillboardRankingWithLyrics" + str(year) + ".csv"
    with open(ranking_data_file_name, 'r', encoding='utf-8') as ranking_file:
        csv_ranking_file_reader = csv.DictReader(ranking_file, delimiter='~')

        billboard_ranking_statistics_directory = "../billboard_ranking_statistics"
        os.chdir(billboard_ranking_statistics_directory)
        ranking_statistics_file_name = "BillboardRankingStatisticsGenre" + str(year) + ".csv"
        with open(ranking_statistics_file_name, 'w', encoding='utf-8') as ranking_statistics_file:
            csv_ranking_file_writer = csv.writer(ranking_statistics_file, delimiter='~')
            csv_ranking_file_writer.writerow(["Genre", "Year", "Word Number", "Syllable Number",
                                              "Average Syllables Per Word", "Average Word Length", "Flesch Reading Ease",
                                              "Flesch-Kincaid Grade Level",  "Fog Scale", "Text Standard"])
            all_lyrics_for_genre = {}
            for row in csv_ranking_file_reader:
                if row["Genre"] in all_lyrics_for_genre.keys():
                    all_lyrics_for_genre[row["Genre"]] = all_lyrics_for_genre[row["Genre"]] + " " + row["Lyrics"]
                else:
                    all_lyrics_for_genre[row["Genre"]] = row["Lyrics"]

            for key in all_lyrics_for_genre:
                # print(textstat.lexicon_count(row))
                # print(textstat.syllable_count(row["Lyrics"]))
                # print(textstat.avg_syllables_per_word(row["Lyrics"]))
                # print(textstat.avg_letter_per_word(row["Lyrics"]))
                stats = [str(key), str(year),
                         str(textstat.lexicon_count(all_lyrics_for_genre[key])),
                         str(textstat.syllable_count(all_lyrics_for_genre[key])),
                         str(textstat.avg_syllables_per_word(all_lyrics_for_genre[key])),
                         str(textstat.avg_letter_per_word(all_lyrics_for_genre[key])),
                         str(textstat.flesch_reading_ease(all_lyrics_for_genre[key])),
                         str(textstat.flesch_kincaid_grade(all_lyrics_for_genre[key])),
                         str(textstat.gunning_fog(all_lyrics_for_genre[key])),
                         str(textstat.text_standard(all_lyrics_for_genre[key]))]
                csv_ranking_file_writer.writerow(stats)

            billboard_ranking_songs_directory = "../billboard_ranking_with_song_genre"
            os.chdir(billboard_ranking_songs_directory)


countSyllables()
