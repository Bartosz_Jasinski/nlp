from termcolor import colored


def testPrints(string_to_print):
    printInfo(string_to_print)
    printWarning(string_to_print)
    printError(string_to_print)


def printInfo(string_to_print):
    printColoredText(string_to_print, 'green')


def printWarning(string_to_print):
    printColoredText(string_to_print, 'yellow')


def printError(string_to_print):
    printColoredText(string_to_print, 'red')


def printColoredText(string_to_print, color):
    print(colored(string_to_print, color), "\n")

