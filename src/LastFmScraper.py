import requests
import json
import os
import csv


def getSongGenre():
    working_directory = "../data/billboard_ranking"
    os.chdir(working_directory)
    billboard_ranking_directory = "../billboard_ranking"
    billboard_ranking_with_song_genre_directory = "../billboard_ranking_with_song_genre"

    first_rank_year = 1960
    last_rank_year = 2018
    for rank_year in range(first_rank_year, last_rank_year):
        printPrettyInfoAboutProcessedPage(rank_year)
        ranking_data_file_name = "BillboardRanking" + str(rank_year) + ".csv"
        ranking_data_file = open(ranking_data_file_name, 'w', encoding='utf-8')
        getSongArtistAndTitle(ranking_data_file)

    last_fm_response = requests.post(last_fm_url)
    d = json.loads(last_fm_response.text)
    print(json.dumps(d, indent=4, sort_keys=True))


def printPrettyInfoAboutProcessedPage(rank_year):
    print('---------------------', rank_year, '---------------------', '\n')


def getSongArtistAndTitle(ranking_data_file):
    with open(ranking_data_file) as billboard_ranking:
        billboard_ranking.close()


def genreLastFmUrl(artist, song_title):
    return 'http://ws.audioscrobbler.com/2.0/?method=track.getinfo&api_key=b25b959554ed76058ac220b7b2e0a026&format=json&artist=' + artist + '&track=' + song_title


getSongGenre()


# def iterateThroughAllBillboardRankings():
#     working_directory = "../data/billboard_ranking"
#     os.chdir(working_directory)
#
#     first_rank_year = 1960
#     # first_rank_year = 1980
#     last_rank_year = 2018
#     for rank_year in range(first_rank_year, last_rank_year):
#         printPrettyInfoAboutProcessedPage(rank_year)
#         ranking_table_html = getWikipediaBillboardRankForGivenYear(rank_year)
#         ranking_data_file_name = "BillboardRanking" + str(rank_year) + ".csv"
#         ranking_data_file = open(ranking_data_file_name, 'w', encoding='utf-8')
#         createRankFile(ranking_data_file, ranking_table_html)
#
#

#
# def getWikipediaBillboardRankForGivenYear(rank_year):
#     wikipedia_page_title = 'Billboard Year-End Hot 100 singles of ' + str(rank_year)
#     billboard_ranking = wikipedia.page(wikipedia_page_title)
#     page_text = billboard_ranking.html()
#     soup = BeautifulSoup(page_text, "html.parser")
#     ranking_table_html = soup.find("table", {"class": "wikitable sortable"})
#     return ranking_table_html
#
#
# def createRankFile(ranking_data_file, ranking_table_html):
#     for row in ranking_table_html.findAll("tr"):
#         cells = row.findAll("td")
#         after_1981_cells = row.findAll("th")  # It is necessary shit
#         # For each "tr", assign each "td" to a variable.
#         rank = ""
#         title = ""
#         artist = ""
#         if len(cells) == 3:
#             rank = cells[0].find(text=True)
#             title = cells[1].findAll(text=True)
#             title = title[int((len(title) - 1) / 2)]
#             artist = cells[2].find(text=True)
#             if title.startswith('"') and title.endswith('"'):
#                 title = title[1:-1]
#
#         elif len(cells) == 2:  # Tables are written inconsistent compare
#             # https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1981 and
#             # https://en.wikipedia.org/wiki/Billboard_Year-End_Hot_100_singles_of_1982
#             rank = after_1981_cells[0].find(text=True)
#             rank = rank.strip()
#             title = cells[0].findAll(text=True)
#             title = title[int((len(title) - 1) / 2)]
#             artist = cells[1].find(text=True)
#             artist = artist.strip()
#             if title.startswith('"') and title.endswith('"'):
#                 title = title[1:-1]
#
#         record = rank + ',' + title + ',' + artist + '\n'
#         # record = record.decode('utf-8', 'ignore').encode("utf-8")
#         if len(rank) != 0 or len(title) != 0 or len(artist) != 0:
#             ranking_data_file.write(record)
#
#     ranking_data_file.close()
# 4
# # !!!!!!!!!!!!!!!!!!DEBUG!!!!!!!!!!!!!!!!!!
# #    print("\n \n \n \n \n \n \n \n \n \n \n \n")
# #    print(ranking_table_html)
# # !!!!!!!!!!!!!!!!!!DEBUG!!!!!!!!!!!!!!!!!!
#
#
# # def clearScrapedStrings(string):
#
#
